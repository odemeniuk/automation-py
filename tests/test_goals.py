import pytest


# def test_strategic_goal_creation_overview_tab(py, gop):
#     gop.goals.navigate.to_my_pods('NextGenPod!')
#     gop.goals.navigate.to_overview_tab()
#     gop.goals.set_goal_overview_tab('Strategic goal', 'Autonomy L1', 'Just a Goal', 'Really Important')
#
#
# def test_strategic_goal_creation_goal_tab(py, gop):
#     gop.goals.navigate.to_my_pods('NextGenPod')
#     gop.my_pod.navigate.to_goals_tab()
#     gop.goals.set_goal_goal_tab('Strategic goal', 'TestingViewOption', 'New Goal', 'Really Important Description')
#
#
# def test_tactical_goal_creation_overview_tab(py, gop):
#     gop.goals.navigate.to_my_pods('NextGenPod')
#     gop.goals.navigate.to_overview_tab()
#     gop.goals.set_goal_overview_tab('Tactical goal', 'Best of Best', 'My Tactical Goal', 'Testing Description')
#
#
# def test_tactical_goal_creation_goal_tab(py, gop):
#     gop.goals.navigate.to_my_pods('NextGenPod')
#     gop.my_pod.navigate.to_goals_tab()
#     gop.goals.set_goal_goal_tab('Tactical goal', 'Best of Best', 'My Goal', 'Testing Description')
#
#
# def test_edit_goal(py, gop):
#     gop.goals.navigate.to_my_pods('NextGenPod')
#     gop.goals.navigate.to_overview_tab()
#     gop.goals.edit_goal_overview_tab('Tactical goal',
#                                      'Best of Best', 'My Goal',
#                                      'Testing Description',
#                                      'Edit Name', 'Edit Description', 'The Accepter')
