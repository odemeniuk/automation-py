import os
import pytest
from src.pages.gop import GopPages
from dotenv import load_dotenv
load_dotenv()



# # TEST SET UP > executed per each test

@pytest.fixture
def gop(py):
    return GopPages(py,)


@pytest.fixture(scope='function', autouse=True)
def setup(gop):
    gop.login.goto(os.environ.get("URL"))
    gop.login.login_as(os.environ.get("ADMIN_LOGIN"), os.environ.get("ADMIN_PASSWORD"))

