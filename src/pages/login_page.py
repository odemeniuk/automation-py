from pylenium import Pylenium

from src.pages.feed_page import FeedPage


class LoginPage:

    # Locators

    USER_NAME_FIELD = '#username'
    PASSWORD_FIELD = '#password'
    LOGIN_BUTTON = '#kc-login'

    # Initializer
    def __init__(self, py: Pylenium):
        self.py = py

    def goto(self, url):
        return self.py.visit(url)

    def login_as(self, user_name, password) -> 'FeedPage':
        self.py.get(self.USER_NAME_FIELD).type(user_name)
        self.py.get(self.PASSWORD_FIELD).type(password)
        self.py.get(self.LOGIN_BUTTON).click()
        return FeedPage(self.py)
