from pylenium import Pylenium

from src.pages.feed_page import FeedPage
from src.pages.goals import GoalsPage
from src.pages.login_page import LoginPage
from src.pages.my_pods.my_pod import MyPodPage


class GopPages:

    def __init__(self, py: Pylenium):
        self.py = py
        self.feed = FeedPage(py)
        self.login = LoginPage(py)
        self.goals = GoalsPage(py)
        self.my_pod = MyPodPage(py)


