from pylenium import Pylenium

from src.pages.navigation import Navigation


class PageBase:
    def __init__(self, py: Pylenium):
        self.py = py
        self.navigate = Navigation(py)
