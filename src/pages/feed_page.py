from src.pages.page_base import PageBase


class FeedPage(PageBase):
    def __init__(self, py):
        super().__init__(py)

    def go_feed(self):
        self.navigate.to_feed()
