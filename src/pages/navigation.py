from pylenium import Pylenium


class Navigation:

    def __init__(self, py: Pylenium):
        self.py = py

    # Locators

    MY_PODS = "#my-pods"
    GOAL_TAB = "//*[@class ='nav']//*[contains(text(), 'Goals')]"
    OVERVIEW_TAB = "//*[@class ='nav']//*[contains(text(), 'Overview')]"

    def to_feed(self):
        self.py.getx("//*[text()='Feed']").click()

    def to_my_pods(self, pod_name: str):
        self.py.get(self.MY_PODS).click()
        self.py.contains(pod_name).should().be_visible().click()

    def to_goals_tab(self):
        self.py.contains('Goals').should().be_visible()
        self.py.getx(self.GOAL_TAB).click()


    def to_overview_tab(self):
        self.py.contains('Overview').should().be_visible()
        self.py.getx(self.OVERVIEW_TAB).click()


