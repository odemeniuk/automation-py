from pylenium import Pylenium

from src.pages.page_base import PageBase


class MyPodPage(PageBase):
    def __init__(self, py: Pylenium):
        super().__init__(py)



    def verify_my_pod_info(self):
        pass

    def verify_pod_info(self):
        pass

    def request_award_button(self):
        pass

    def select_tab(self, tab_name: str):
        pass

    def edit_pod_mission(self):
        pass

    def open_edit_form(self):
        pass

    def get_pod_name(self):
        pass
