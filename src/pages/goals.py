from pylenium import Pylenium
from selenium.common.exceptions import TimeoutException

from src.pages.page_base import PageBase


class GoalsPage(PageBase):
    def __init__(self, py: Pylenium):
        super().__init__(py)




    # Locators

    WEEKS = {'1WK': '[1]', '2WK': '[2]', '3WK': '[3]'}
    ACTIVE_GOAL = "//*[@class='pod-page-goals ng-star-inserted']"
    GOALS_ACTIVE_LIST = "//*[@class='pod-goals-container ng-star-inserted']"
    GOAL_STATUS = "//*[@class='pod-goals-status']"
    SET_GOAL = "//*[contains(text(), 'Set a goal')]"
    NO_GOALS_SECTION = "//*[@class='no-goal ng-star-inserted']"
    NO_ACTIVE_GOALS = "//*[contains(text(), 'This pod has no active goals.')]"
    NO_FINISHED_GOALS = "//*[contains(text(), 'This pod has no finished goals')]"
    GOALS_FINISHED_HEADER = "//*[@id='mat-expansion-panel-header-0']"
    GOAL_WEEK = "//*[@role='option']"
    SET_GOAL_NOW_BUTTON = "//button[@class ='normal-regular-primary-button']"
    SET_GOAL_BUTTON = "//*[@class='normal-regular-primary-button set-goal-button']"
    CANCEL_GOAL_BUTTON = "//*[@class='cancel-button error-regular-primary-button ng-star-inserted']"
    SAVE_GOAL_BUTTON = "//*[@class='continue normal-regular-primary-button ng-star-inserted']"
    EDIT_GOAL_OPTION = "//a[@role='menuitem'][contains(text(), ' Edit goal ')]"
    CANCEL_GOAL_OPTION = "//a[@role='menuitem'][contains(text(), 'Cancel')]"

    GOAL_MENU = "(//material-icons[@class='mat-menu-trigger dots'])"
    GOAL_TYPE = "[formcontrolname='goalType']"
    STRATEGIC_GOAL = "//*[text()='Strategic goal']"
    TACTICAL_GOAL = "//*[text()='Tactical goal']"
    GOAL_AWARDS = "[formcontrolname='goalAwards']"
    SELECT_AWARD = "app-send-award-large-list-item"
    GOAL_NAME = "[formcontrolname='goalName']"
    GOAL_DESCRIPTION = "[formcontrolname='goalDescription']"
    GOAL_TIME_FRAME = "[formcontrolname='goalTimeFrame']"
    CANCEL_DESCRIPTION = "[formcontrolname='cancelDescription']"
    GOAL_SETTING_CARD = "div[@class ='set-goal-card-header']"

    GOAL_AREA = "//div[@class ='pod-page-goals ng-star-inserted']"
    GOAL_BADGE = "//img[@class ='pod-goals-small-badge-gray']"
    TEXT_DESCRIPTION = "//p[@class ='pod-description-of-goal']"
    STRATEGIC_TYPE = "//p[@class='pod-type-of-goal' and contains(text(),'Strategic')]"
    NAME_OF_GOAL = "//h3[@class='pod-name-of-goal']"
    TOTAL_XP = "(//p[@class='pod-type-of-goal'])[2]"
    TOTAL_WEEKS = "//p[@class='pod-type-of-goal' and contains(text(),'WEEKS')]"
    STARTING_DATE = "//p[@class='pod-starting-date']"
    GOAL_SUCCESS = "//div[@class='message-container' and contains(text(),'Goal successfully created!')]"

    def verify_goal_info_goal_tab(self, goal_name, goal_description):
        self.py.getx(self.ACTIVE_GOAL).should().be_visible()
        self.py.getx(self.GOAL_BADGE).should().be_visible()
        self.py.getx(self.TEXT_DESCRIPTION).should().have_text(goal_description)
        self.py.getx(self.NAME_OF_GOAL).should().have_text(goal_name)

    def verify_goal_info_overview_tab(self, goal_name):
        self.py.getx("//span[@class='goal-selected__type']").should().contain_text('GOAL')
        self.py.getx("//span[@class='goal-selected__points']").should().be_visible()
        self.py.getx("//h3[@class='goal-selected__title']").should().have_text(goal_name)
        self.py.getx("//div[@class='goal-selected__description']").should().be_visible()
        self.py.getx("//span[@class='progress-bar__text']").should().be_visible()
        self.py.getx("//div[@class='goal-selected__awards']").should().be_visible()

    def set_goal_overview_tab(self, goal_type: str, goal_award: str, goal_name: str, goal_description: str):
        try:
            print('Create new goal')
            self.click_set_goal_now_button()
            self.input_goal_form(goal_type, goal_award, goal_name, goal_description)
        except TimeoutException:
            print('Verifying goal')
            self.verify_goal_info_overview_tab(goal_name)
        finally:
            print('Canceling goal')
            self.verify_goal_info_overview_tab(goal_name)
            self.cancel_goal()
            return self

    def set_goal_goal_tab(self, goal_type: str, goal_award: str, goal_name: str, goal_description: str):
        try:
            print('Create new goal')
            self.click_set_a_goal_link()
            self.input_goal_form(goal_type, goal_award, goal_name, goal_description)
        except TimeoutException:
            print('Verifying goal')
            self.verify_goal_info_goal_tab(goal_name, goal_description)
        finally:
            print('Canceling goal')
            self.verify_goal_info_goal_tab(goal_name, goal_description)
            self.cancel_goal()
            return self

    def input_goal_form(self, goal_type, goal_award, goal_name: str, goal_description: str):
        self.py.get(self.GOAL_TYPE).should().be_visible().click()
        self.py.getx("//*[text()='" + goal_type + "']").is_displayed()
        self.py.getx("//*[text()='" + goal_type + "']").click()
        self.py.get(self.GOAL_NAME).type(goal_name)
        self.py.get(self.GOAL_DESCRIPTION).type(goal_description)
        self.py.get(self.GOAL_AWARDS).type(goal_award).click()
        # self.py.contains(goal_award).should().be_visible().click()
        self.py.get(self.SELECT_AWARD).should().be_enabled().click()
        self.py.get(self.GOAL_TIME_FRAME).click()
        self.py.getx(self.GOAL_WEEK + self.WEEKS['2WK']).click()
        self.click_save_goal_button()
        return self

    def input_cancel_goal_form(self):
        self.click_cancel_menu()
        self.py.get(self.CANCEL_DESCRIPTION).type('Deleting')
        self.py.getx(self.CANCEL_GOAL_BUTTON).click()
        return self

    def input_edit_goal_form(self, edit_name: str, edit_description: str, edit_award: str):
        self.py.get(self.GOAL_NAME).clear().type(edit_name)
        self.py.get(self.GOAL_DESCRIPTION).clear().type(edit_description)
        self.py.get(self.GOAL_AWARDS).type(edit_award).click()
        self.py.contains(edit_award).should().be_visible().click()
        return self

    def cancel_goal(self):
        # Cancel Goal Menu
        try:
            self.py.getx(self.ACTIVE_GOAL)
            self.navigate.to_overview_tab()
            self.input_cancel_goal_form()
        except TimeoutException:
            # self.py.getx(self.GOAL_SETTING_CARD).is_displayed()
            self.input_cancel_goal_form()
        finally:
            self.py.getx(self.SET_GOAL_NOW_BUTTON).is_displayed()
            assert self.py.contains('You have not set any goals yet')
        return self

    def edit_goal(self, edit_name, edit_description, edit_award):
        try:
            self.py.getx(self.ACTIVE_GOAL)
            self.navigate.to_overview_tab()
            self.click_edit_menu()
            self.input_edit_goal_form(edit_name, edit_description, edit_award)
            self.click_save_goal_button()
        except TimeoutException:
            self.click_edit_menu()
            self.input_edit_goal_form(edit_name, edit_description, edit_award)
            self.click_save_goal_button()
        return self

    def edit_goal_overview_tab(self, goal_type,
                               goal_award, goal_name,
                               goal_description, edit_name, edit_description, edit_award):
        try:
            self.edit_goal(edit_name, edit_description, edit_award)
        except TimeoutException:
            self.click_set_goal_now_button()
            self.input_goal_form(goal_type, goal_award, goal_name, goal_description)
            self.edit_goal(edit_name, edit_description, edit_award)
        finally:
            self.py.contains(edit_award)
            self.cancel_goal()
        return self

    def click_set_goal_now_button(self):
        # self.py.getx(self.SET_GOAL_NOW_BUTTON).is_displayed()
        self.py.getx(self.SET_GOAL_NOW_BUTTON).click()
        return self

    def click_set_a_goal_link(self):
        self.py.getx(self.SET_GOAL).click()
        return self

    def click_save_goal_button(self):
        self.py.getx(self.SAVE_GOAL_BUTTON).should().be_enabled()
        self.py.getx(self.SAVE_GOAL_BUTTON).click()
        return self

    def click_edit_menu(self):
        self.py.getx(self.GOAL_MENU + '[2]').click()
        self.py.getx(self.EDIT_GOAL_OPTION).should().be_visible().click()
        return self

    def click_cancel_menu(self):
        self.py.getx(self.GOAL_MENU + '[2]').click()
        self.py.getx(self.CANCEL_GOAL_OPTION).should().be_visible().click()
        return self

    # def verify_active_goal(self):
    #     self.py.getx(self.ACTIVE_GOAL).is_displayed()
    #     return self

    # assert self.py.contains('You have not set any goals yet')
